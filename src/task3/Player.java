package task3;

public class Player implements Playable, Recordable {

    @Override
    public void play() {
        System.out.println("Playable - play");
    }

    @Override
    public void pause() {
        System.out.println("Playable - pause");
    }

    @Override
    public void stop() {
        System.out.println("Playable - stop");
    }

    @Override
    public void record() {
        System.out.println("Recordable - record");
    }

    @Override
    public void pauseRecord() {
        System.out.println("Recordable - pause");
    }

    @Override
    public void stopRecord() {
        System.out.println("Recordable - stop");
    }
}
