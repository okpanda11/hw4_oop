package task3;

public class Main {
    public static void main(String[] args) {

        Player myPlayer = new Player();
        // реазилация методов интерфейса Playable
        myPlayer.play();
        myPlayer.pause();
        myPlayer.stop();

        System.out.println();
        // реазилация методов интерфейса Recordable
        myPlayer.record();
        myPlayer.pauseRecord();
        myPlayer.stopRecord();
    }
}
