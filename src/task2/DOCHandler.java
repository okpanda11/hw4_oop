package task2;

public class DOCHandler extends AbstractHandler {

    @Override
    protected void open() {
        System.out.println("DOC open");
    }

    @Override
    protected void create() {
        System.out.println("DOC create");
    }

    @Override
    protected void change() {
        System.out.println("DOC change");
    }

    @Override
    protected void save() {
        System.out.println("DOC save");
    }
}
