package task2;

public class TXTHandler extends AbstractHandler {

    @Override
    protected void open() {
        System.out.println("TXT open");
    }

    @Override
    protected void create() {
        System.out.println("TXT create");
    }

    @Override
    protected void change() {
        System.out.println("TXT change");
    }

    @Override
    protected void save() {
        System.out.println("TXT save");
    }
}
