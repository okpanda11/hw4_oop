package task2;

public class XMLHandler extends AbstractHandler {
    @Override
    protected void open() {
        System.out.println("XML open");
    }

    @Override
    protected void create() {
        System.out.println("XML create");
    }

    @Override
    protected void change() {
        System.out.println("XML change");
    }

    @Override
    protected void save() {
        System.out.println("XML save");
    }
}
