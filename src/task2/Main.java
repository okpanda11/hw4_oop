package task2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("input the number of type: \n1 - XML\n2 - TXT\n3 - DOC");
        int typeNumber = sc.nextInt();
        switch (typeNumber) {
            case 1 -> {
                AbstractHandler XML_doc = new XMLHandler();
                XML_doc.open();
                XML_doc.create();
                XML_doc.change();
                XML_doc.save();
            }
            case 2 -> {
                AbstractHandler TXT_doc = new TXTHandler();
                TXT_doc.open();
                TXT_doc.create();
                TXT_doc.change();
                TXT_doc.save();
            }
            case 3 -> {
                AbstractHandler DOC_doc = new DOCHandler();
                DOC_doc.open();
                DOC_doc.create();
                DOC_doc.change();
                DOC_doc.save();
            }
            default -> System.out.println("incorrect choice");
        }
        sc.close();
    }
}
