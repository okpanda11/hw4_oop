package task2;

public abstract class AbstractHandler {
    protected abstract void open();
    protected abstract void create();
    protected abstract void change();
    protected abstract void save();
}
